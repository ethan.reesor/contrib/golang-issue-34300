# Golang Issue

## Issues with `-trimpath`

  1. Build from the parent directory - builds
     - `go build -trimpath -o a.exe ./a/b`
  2. Build from the module directory - builds
     - `cd ./a && go build -trimpath -o ../b.exe ./b`
  3. Build from the parent directory, forcing modules - fails
     - `GO111MODULE=on go build -trimpath -o am.exe ./a/b`

Step 3 fails with:

```
go: cannot find main module, but found .git/config in C:\Source\test
        to create a module there, run:
        go mod init
```

## Issues with dependencies

  1. Build from the parent directory - fails
     - `go build ./c/d`
  2. Build from the module directory - builds
     - `cd ./c && go build ./d`
  3. Build from the parent directory, forcing modules - fails
     - `GO111MODULE=on go build ./c/d`

Step 1 fails with:

```
c\d\main.go:3:8: cannot find package "github.com/etcd-io/bbolt" in any of:
        GOROOT\src\github.com\etcd-io\bbolt (from $GOROOT)
        GOPATH\src\github.com\etcd-io\bbolt (from $GOPATH)
```

Step 3 fails with:

```
go: cannot find main module, but found .git/config in C:\Source\test
        to create a module there, run:
        go mod init
```