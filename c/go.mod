module test

go 1.12

require (
	github.com/etcd-io/bbolt v1.3.3
	go.etcd.io/bbolt v1.3.3 // indirect
	golang.org/x/sys v0.0.0-20190913121621-c3b328c6e5a7 // indirect
)
